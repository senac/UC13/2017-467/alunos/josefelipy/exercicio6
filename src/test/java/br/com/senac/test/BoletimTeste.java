/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.test;

import br.com.senac.ex6.Aluno;
import br.com.senac.ex6.VerificarBoletim;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Administrador
 */
public class BoletimTeste {

    public BoletimTeste() {
    }

    @Test
    public void alunoDeveSerAprovado() {
        VerificarBoletim boletim = new VerificarBoletim();
        Aluno aluno = new Aluno("José", 1);
        aluno.setNota(10);

        boolean resultado = boletim.isAprovado(aluno);
        assertTrue(resultado);

    }

    @Test
    public void alunoNaoDeveSerAprovado() {
        VerificarBoletim boletim = new VerificarBoletim();
        Aluno aluno = new Aluno("José", 1);
        aluno.setNota(2);

        boolean resultado = boletim.isAprovado(aluno);
        assertFalse(resultado);

    }

    @Test
    public void alunoDeveSerReprovado() {
        VerificarBoletim boletim = new VerificarBoletim();
        Aluno aluno = new Aluno("Felipy", 2);
        aluno.setNota(2);

        boolean resultado = boletim.isReprovado(aluno);
        assertTrue(resultado);

    }

    @Test
    public void alunoNaoDeveSerReprovado() {
        VerificarBoletim boletim = new VerificarBoletim();
        Aluno aluno = new Aluno("Felipy", 2);
        aluno.setNota(10);

        boolean resultado = boletim.isReprovado(aluno);
        assertFalse(resultado);
    }

    @Test
    public void alunoDeveFicarDeRecuperacao() {
        VerificarBoletim boletim = new VerificarBoletim();
        Aluno aluno = new Aluno("Segatto", 3);
        aluno.setNota(5);

        boolean resultado = boletim.isRecuperacao(aluno);
        assertTrue(resultado);
    }

    @Test
    public void alunosNaoDeveFicarDeRecuperacao() {
        VerificarBoletim boletim = new VerificarBoletim();
        Aluno jose = new Aluno("José", 1);
        jose.setNota(10);
        Aluno felipy = new Aluno("Felipy", 2);
        felipy.setNota(2);

        boolean resultadoJose = boletim.isRecuperacao(jose);
        boolean resultadoFelpy = boletim.isRecuperacao(felipy);
        assertFalse(resultadoJose);
        assertFalse(resultadoFelpy);
    }

    @Test
    public void alunoDeveReceberMensagemAprovado() {
        VerificarBoletim boletim = new VerificarBoletim();
        Aluno aluno = new Aluno("José", 1);
        aluno.setNota(10);
        String resultado = boletim.resultadoAprovacao(aluno);
        assertEquals(boletim.ALUNO_APROVADO, boletim.resultadoAprovacao(aluno));
    }

    @Test
    public void alunoDeveReceberMensagemReprovado() {
        VerificarBoletim boletim = new VerificarBoletim();
        Aluno aluno = new Aluno("Felpy", 2);
        aluno.setNota(2);
        String resultado = boletim.resultadoAprovacao(aluno);
        assertEquals(boletim.ALUNO_REPROVADO, boletim.resultadoAprovacao(aluno));
    }

    @Test
    public void alunoDeveReceberMensagemRecuperacao() {
        VerificarBoletim boletim = new VerificarBoletim();
        Aluno aluno = new Aluno("Segatto", 3);
        aluno.setNota(5);
        String resultado = boletim.resultadoAprovacao(aluno);
        assertEquals(boletim.ALUNO_RECUPERACAO, boletim.resultadoAprovacao(aluno));
    }
}
