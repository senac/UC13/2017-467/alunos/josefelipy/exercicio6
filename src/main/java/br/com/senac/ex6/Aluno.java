
package br.com.senac.ex6;


public class Aluno {
    
private String nome;
private int nota;
private int codigoAluno;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getNota() {
        return nota;
    }

    public void setNota(int nota) {
        this.nota = nota;
    }

    public int getCodigoAluno() {
        return codigoAluno;
    }

    public void setCodigoAluno(int codigoAluno) {
        this.codigoAluno = codigoAluno;
    }

    public Aluno(String nome, int codigoAluno) {
        this.nome = nome;
        this.codigoAluno = codigoAluno;
    }

    public Aluno() {
    }


 
    
}
