
package br.com.senac.ex6;

public class VerificarBoletim {
    
    public static String ALUNO_APROVADO = "Aprovado";
    public static String ALUNO_REPROVADO = "Reprovado";
    public static String ALUNO_RECUPERACAO = "Recuperação";

    public VerificarBoletim() {
    }

   public boolean isAprovado(Aluno aluno){
       
       return aluno.getNota() >=7  ;
   }
   
   public boolean isReprovado(Aluno aluno){
       
       return aluno.getNota() <4 ;
   }
   
   public boolean isRecuperacao (Aluno aluno){
       
       return aluno.getNota() >=4 && aluno.getNota() <= 6 ;
   }
   
   public String resultadoAprovacao (Aluno aluno){
       String resultado= null;
       
       if(isAprovado(aluno)){
           resultado = this.ALUNO_APROVADO ;
       }else if (isReprovado(aluno)){
           resultado = this.ALUNO_REPROVADO;
       }else 
           resultado = this.ALUNO_RECUPERACAO ;
       
       
       return resultado;
   }
}

